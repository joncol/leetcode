# LeetCode

My personal LeetCode solutions.

## Using `leetcode-cli`

To log in with `leetcode-cli`, copy the cookie from the last `/graphql` request
(via the developer tools in a browser), and use:

```bash
leetcode user -c
```

Enter your LeetCode username and paste the full cookie. You should now be logged
in.

To list easy, unlocked, unsolved problems:
```bash
leetcode list -q eLD
```

To get a new problem (in this example, the problem ID is 9):
```bash
leetcode show 9 -gx -o src
```
