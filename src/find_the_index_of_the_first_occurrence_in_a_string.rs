use crate::Solution;

/*
 * @lc app=leetcode id=28 lang=rust
 *
 * [28] Find the Index of the First Occurrence in a String
 *
 * https://leetcode.com/problems/find-the-index-of-the-first-occurrence-in-a-string/description/
 *
 * algorithms
 * Easy (41.62%)
 * Likes:    5429
 * Dislikes: 353
 * Total Accepted:    2.3M
 * Total Submissions: 5.4M
 * Testcase Example:  '"sadbutsad"\n"sad"'
 *
 * Given two strings needle and haystack, return the index of the first
 * occurrence of needle in haystack, or -1 if needle is not part of
 * haystack.
 *
 *
 * Example 1:
 *
 *
 * Input: haystack = "sadbutsad", needle = "sad"
 * Output: 0
 * Explanation: "sad" occurs at index 0 and 6.
 * The first occurrence is at index 0, so we return 0.
 *
 *
 * Example 2:
 *
 *
 * Input: haystack = "leetcode", needle = "leeto"
 * Output: -1
 * Explanation: "leeto" did not occur in "leetcode", so we return -1.
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= haystack.length, needle.length <= 10^4
 * haystack and needle consist of only lowercase English characters.
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn str_str(haystack: String, needle: String) -> i32 {
        let t = haystack.as_bytes();
        let p = needle.as_bytes();
        let n = t.len();
        let m = p.len();
        let prefix_fun = Self::compute_prefix_function(&needle);
        let mut q: usize = 0;
        for i in 0..n {
            while q > 0 && p[q] != t[i] {
                q = prefix_fun[q - 1];
            }
            if p[q] == t[i] {
                q += 1;
            }
            if q == m {
                return (i + 1 - m) as i32;
            }
        }
        -1
    }

    fn compute_prefix_function(pattern: &String) -> Vec<usize> {
        let p = pattern.as_bytes();
        let m = p.len();
        let mut prefix_fun = Vec::with_capacity(m);
        prefix_fun.resize(m, 0);
        let mut k: usize = 0;
        for q in 1..m {
            while k > 0 && p[k] != p[q] {
                k = prefix_fun[k - 1];
            }
            if p[k] == p[q] {
                k += 1;
            }
            prefix_fun[q] = k;
        }
        prefix_fun
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(
            Solution::str_str("sadbutsad".to_string(), "sad".to_string()),
            0
        );
    }

    #[test]
    fn test2() {
        assert_eq!(
            Solution::str_str("leetcode".to_string(), "leeto".to_string()),
            -1
        );
    }

    #[test]
    fn test3() {
        assert_eq!(
            Solution::str_str("leetcode".to_string(), "tco".to_string()),
            3
        );
    }

    #[test]
    fn test4() {
        let prefix_len = 10e4 as i32;
        let mut long_haystack = (0..prefix_len).map(|_| "x").collect::<String>();
        long_haystack.push_str("abc");
        assert_eq!(
            Solution::str_str(long_haystack, "abc".to_string()),
            prefix_len
        );
    }

    #[test]
    fn test5() {
        assert_eq!(
            Solution::str_str("aaaaa".to_string(), "bba".to_string()),
            -1
        );
    }

    #[test]
    fn test6() {
        assert_eq!(
            Solution::str_str("mississippi".to_string(), "issip".to_string()),
            4
        );
    }
}
