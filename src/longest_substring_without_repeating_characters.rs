use crate::Solution;

/*
 * @lc app=leetcode id=3 lang=rust
 *
 * [3] Longest Substring Without Repeating Characters
 *
 * https://leetcode.com/problems/longest-substring-without-repeating-characters/description/
 *
 * algorithms
 * Medium (34.44%)
 * Likes:    38743
 * Dislikes: 1805
 * Total Accepted:    5.5M
 * Total Submissions: 15.9M
 * Testcase Example:  '"abcabcbb"'
 *
 * Given a string s, find the length of the longest substring without repeating
 * characters.
 *
 *
 * Example 1:
 *
 *
 * Input: s = "abcabcbb"
 * Output: 3
 * Explanation: The answer is "abc", with the length of 3.
 *
 *
 * Example 2:
 *
 *
 * Input: s = "bbbbb"
 * Output: 1
 * Explanation: The answer is "b", with the length of 1.
 *
 *
 * Example 3:
 *
 *
 * Input: s = "pwwkew"
 * Output: 3
 * Explanation: The answer is "wke", with the length of 3.
 * Notice that the answer must be a substring, "pwke" is a subsequence and not
 * a substring.
 *
 *
 *
 * Constraints:
 *
 *
 * 0 <= s.length <= 5 * 10^4
 * s consists of English letters, digits, symbols and spaces.
 *
 *
 */

// @lc code=start
use std::collections::HashSet;

impl Solution {
    pub fn length_of_longest_substring(s: String) -> i32 {
        if s.is_empty() {
            return 0;
        }
        let mut i = 0;
        let mut j = 0;
        let mut max_len = 0;
        let mut visited: HashSet<u8> = HashSet::new();
        let bytes = s.as_bytes();
        while j < bytes.len() {
            if visited.contains(&bytes[j]) {
                if j - i > max_len {
                    max_len = j - i;
                }
                i += 1;
                j = i;
                visited.clear();
            } else {
                visited.insert(bytes[j]);
                j += 1;
                if j - i > max_len {
                    max_len = j - i;
                }
            }
        }
        max_len as i32
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(
            Solution::length_of_longest_substring("abcabcbb".to_string()),
            3
        );
    }

    #[test]
    fn test2() {
        assert_eq!(
            Solution::length_of_longest_substring("bbbbb".to_string()),
            1
        );
    }

    #[test]
    fn test3() {
        assert_eq!(
            Solution::length_of_longest_substring("pwwkew".to_string()),
            3
        );
    }

    #[test]
    fn test4() {
        assert_eq!(Solution::length_of_longest_substring(" ".to_string()), 1);
    }

    #[test]
    fn test5() {
        assert_eq!(Solution::length_of_longest_substring("".to_string()), 0);
    }

    #[test]
    fn test6() {
        assert_eq!(Solution::length_of_longest_substring("au".to_string()), 2);
    }

    #[test]
    fn test7() {
        assert_eq!(Solution::length_of_longest_substring("dvdf".to_string()), 3);
    }
}
