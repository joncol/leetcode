use crate::Solution;

/*
 * @lc app=leetcode id=279 lang=rust
 *
 * [279] Perfect Squares
 *
 * https://leetcode.com/problems/perfect-squares/description/
 *
 * algorithms
 * Medium (53.15%)
 * Likes:    11030
 * Dislikes: 459
 * Total Accepted:    830.7K
 * Total Submissions: 1.5M
 * Testcase Example:  '12'
 *
 * Given an integer n, return the least number of perfect square numbers that
 * sum to n.
 *
 * A perfect square is an integer that is the square of an integer; in other
 * words, it is the product of some integer with itself. For example, 1, 4, 9,
 * and 16 are perfect squares while 3 and 11 are not.
 *
 *
 * Example 1:
 *
 *
 * Input: n = 12
 * Output: 3
 * Explanation: 12 = 4 + 4 + 4.
 *
 *
 * Example 2:
 *
 *
 * Input: n = 13
 * Output: 2
 * Explanation: 13 = 4 + 9.
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= n <= 10^4
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn num_squares(n: i32) -> i32 {
        let n = n as usize;

        let mut perfect_squares = Vec::with_capacity(n);

        for i in 1..=(n as f64).sqrt() as usize {
            perfect_squares.push(i * i);
        }

        let mut s = Vec::with_capacity(n + 1);
        s.resize(n + 1, usize::MAX);
        s[0] = 0;

        for i in 1..=n {
            for &v in perfect_squares.iter() {
                if v > i {
                    break;
                }
                if s[i - v] + 1 < s[i] {
                    s[i] = s[i - v] + 1;
                }
            }
        }
        s[n] as i32
    }
}

// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::num_squares(12), 3);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::num_squares(13), 2);
    }

    #[test]
    fn test3() {
        assert_eq!(Solution::num_squares(2), 2);
    }
}
