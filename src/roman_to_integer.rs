use crate::Solution;

/*
 * @lc app=leetcode id=13 lang=rust
 *
 * [13] Roman to Integer
 *
 * https://leetcode.com/problems/roman-to-integer/description/
 *
 * algorithms
 * Easy (60.53%)
 * Likes:    13404
 * Dislikes: 845
 * Total Accepted:    3.4M
 * Total Submissions: 5.6M
 * Testcase Example:  '"III"'
 *
 * Roman numerals are represented by seven different symbols: I, V, X, L, C, D
 * and M.
 *
 *
 * Symbol       Value
 * I             1
 * V             5
 * X             10
 * L             50
 * C             100
 * D             500
 * M             1000
 *
 * For example, 2 is written as II in Roman numeral, just two ones added
 * together. 12 is written as XII, which is simply X + II. The number 27 is
 * written as XXVII, which is XX + V + II.
 *
 * Roman numerals are usually written largest to smallest from left to right.
 * However, the numeral for four is not IIII. Instead, the number four is
 * written as IV. Because the one is before the five we subtract it making
 * four. The same principle applies to the number nine, which is written as IX.
 * There are six instances where subtraction is used:
 *
 *
 * I can be placed before V (5) and X (10) to make 4 and 9.
 * X can be placed before L (50) and C (100) to make 40 and 90.
 * C can be placed before D (500) and M (1000) to make 400 and 900.
 *
 *
 * Given a roman numeral, convert it to an integer.
 *
 *
 * Example 1:
 *
 *
 * Input: s = "III"
 * Output: 3
 * Explanation: III = 3.
 *
 *
 * Example 2:
 *
 *
 * Input: s = "LVIII"
 * Output: 58
 * Explanation: L = 50, V= 5, III = 3.
 *
 *
 * Example 3:
 *
 *
 * Input: s = "MCMXCIV"
 * Output: 1994
 * Explanation: M = 1000, CM = 900, XC = 90 and IV = 4.
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= s.length <= 15
 * s contains only the characters ('I', 'V', 'X', 'L', 'C', 'D', 'M').
 * It is guaranteed that s is a valid roman numeral in the range [1, 3999].
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn roman_to_int(s: String) -> i32 {
        let mut result = 0;
        let mut ch_iter = s.chars().peekable();
        while let Some(ch) = ch_iter.next() {
            let next_ch = ch_iter.peek();
            match (ch, next_ch) {
                ('I', Some('V')) => {
                    result += 4;
                    ch_iter.next();
                }
                ('I', Some('X')) => {
                    result += 9;
                    ch_iter.next();
                }
                ('I', _) => {
                    result += 1;
                }
                ('V', _) => {
                    result += 5;
                }
                ('X', Some('L')) => {
                    result += 40;
                    ch_iter.next();
                }
                ('X', Some('C')) => {
                    result += 90;
                    ch_iter.next();
                }
                ('X', _) => {
                    result += 10;
                }
                ('L', _) => {
                    result += 50;
                }
                ('C', Some('D')) => {
                    result += 400;
                    ch_iter.next();
                }
                ('C', Some('M')) => {
                    result += 900;
                    ch_iter.next();
                }
                ('C', _) => {
                    result += 100;
                }
                ('D', _) => {
                    result += 500;
                }
                ('M', _) => {
                    result += 1000;
                }
                _ => {}
            }
        }
        result
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::roman_to_int(String::from("III")), 3);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::roman_to_int(String::from("LVIII")), 58);
    }

    #[test]
    fn test3() {
        assert_eq!(Solution::roman_to_int(String::from("MCMXCIV")), 1994);
    }

    #[test]
    fn test4() {
        assert_eq!(Solution::roman_to_int(String::from("IV")), 4);
    }
}
