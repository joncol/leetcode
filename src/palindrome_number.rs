use crate::Solution;

/*
 * @lc app=leetcode id=9 lang=rust
 *
 * [9] Palindrome Number
 *
 * https://leetcode.com/problems/palindrome-number/description/
 *
 * algorithms
 * Easy (55.51%)
 * Likes:    12009
 * Dislikes: 2669
 * Total Accepted:    4.2M
 * Total Submissions: 7.6M
 * Testcase Example:  '121'
 *
 * Given an integer x, return true if x is a palindrome, and false
 * otherwise.
 *
 *
 * Example 1:
 *
 *
 * Input: x = 121
 * Output: true
 * Explanation: 121 reads as 121 from left to right and from right to left.
 *
 *
 * Example 2:
 *
 *
 * Input: x = -121
 * Output: false
 * Explanation: From left to right, it reads -121. From right to left, it
 * becomes 121-. Therefore it is not a palindrome.
 *
 *
 * Example 3:
 *
 *
 * Input: x = 10
 * Output: false
 * Explanation: Reads 01 from right to left. Therefore it is not a
 * palindrome.
 *
 *
 *
 * Constraints:
 *
 *
 * -2^31 <= x <= 2^31 - 1
 *
 *
 *
 * Follow up: Could you solve it without converting the integer to a string?
 */

// @lc code=start
impl Solution {
    pub fn is_palindrome(x: i32) -> bool {
        let s = x.to_string();
        let (h1, h2) = s.split_at(s.len() / 2);
        h1 == &h2.chars().rev().collect::<String>()[..h1.len()]
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::is_palindrome(121), true);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::is_palindrome(-121), false);
    }

    #[test]
    fn test3() {
        assert_eq!(Solution::is_palindrome(10), false);
    }

    #[test]
    fn test4() {
        assert_eq!(Solution::is_palindrome(12321), true);
    }
}
