use crate::Solution;

/*
 * @lc app=leetcode id=1137 lang=rust
 *
 * [1137] N-th Tribonacci Number
 *
 * https://leetcode.com/problems/n-th-tribonacci-number/description/
 *
 * algorithms
 * Easy (63.45%)
 * Likes:    4020
 * Dislikes: 177
 * Total Accepted:    609.1K
 * Total Submissions: 960.1K
 * Testcase Example:  '4'
 *
 * The Tribonacci sequence Tn is defined as follows:
 *
 * T0 = 0, T1 = 1, T2 = 1, and Tn+3 = Tn + Tn+1 + Tn+2 for n >= 0.
 *
 * Given n, return the value of Tn.
 *
 *
 * Example 1:
 *
 *
 * Input: n = 4
 * Output: 4
 * Explanation:
 * T_3 = 0 + 1 + 1 = 2
 * T_4 = 1 + 1 + 2 = 4
 *
 *
 * Example 2:
 *
 *
 * Input: n = 25
 * Output: 1389537
 *
 *
 *
 * Constraints:
 *
 *
 * 0 <= n <= 37
 * The answer is guaranteed to fit within a 32-bit integer, ie. answer <= 2^31
 * - 1.
 *
 */

// @lc code=start
impl Solution {
    pub fn tribonacci(n: i32) -> i32 {
        let n: usize = n as usize;
        if n == 0 {
            return 0;
        } else if n == 1 || n == 2 {
            return 1;
        }
        let mut s = Vec::with_capacity(n + 1);
        s.resize(n + 1, 0);
        s[0] = 0;
        s[1] = 1;
        s[2] = 1;
        for i in 3..=n {
            s[i] = s[i - 1] + s[i - 2] + s[i - 3];
        }
        s[n]
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::tribonacci(4), 4);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::tribonacci(25), 1389537);
    }
}
