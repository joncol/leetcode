use crate::Solution;

/*
 * @lc app=leetcode id=1578 lang=rust
 *
 * [1578] Minimum Time to Make Rope Colorful
 *
 * https://leetcode.com/problems/minimum-time-to-make-rope-colorful/description/
 *
 * algorithms
 * Medium (63.97%)
 * Likes:    3761
 * Dislikes: 132
 * Total Accepted:    250.6K
 * Total Submissions: 392.1K
 * Testcase Example:  '"abaac"\n[1,2,3,4,5]'
 *
 * Alice has n balloons arranged on a rope. You are given a 0-indexed string
 * colors where colors[i] is the color of the i^th balloon.
 *
 * Alice wants the rope to be colorful. She does not want two consecutive
 * balloons to be of the same color, so she asks Bob for help. Bob can remove
 * some balloons from the rope to make it colorful. You are given a 0-indexed
 * integer array neededTime where neededTime[i] is the time (in seconds) that
 * Bob needs to remove the i^th balloon from the rope.
 *
 * Return the minimum time Bob needs to make the rope colorful.
 *
 *
 * Example 1:
 *
 *
 * Input: colors = "abaac", neededTime = [1,2,3,4,5]
 * Output: 3
 * Explanation: In the above image, 'a' is blue, 'b' is red, and 'c' is green.
 * Bob can remove the blue balloon at index 2. This takes 3 seconds.
 * There are no longer two consecutive balloons of the same color. Total time =
 * 3.
 *
 * Example 2:
 *
 *
 * Input: colors = "abc", neededTime = [1,2,3]
 * Output: 0
 * Explanation: The rope is already colorful. Bob does not need to remove any
 * balloons from the rope.
 *
 *
 * Example 3:
 *
 *
 * Input: colors = "aabaa", neededTime = [1,2,3,4,1]
 * Output: 2
 * Explanation: Bob will remove the balloons at indices 0 and 4. Each balloons
 * takes 1 second to remove.
 * There are no longer two consecutive balloons of the same color. Total time =
 * 1 + 1 = 2.
 *
 *
 *
 * Constraints:
 *
 *
 * n == colors.length == neededTime.length
 * 1 <= n <= 10^5
 * 1 <= neededTime[i] <= 10^4
 * colors contains only lowercase English letters.
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn min_cost(colors: String, needed_time: Vec<i32>) -> i32 {
        let colors = colors.bytes().collect::<Vec<u8>>();
        let n = colors.len();
        let mut s = Vec::with_capacity(n);
        s.resize(n, i32::MAX);
        let mut total_cost = 0;
        let mut i = 0;
        while i < n {
            let mut span_size = 0;
            let mut total_span_cost = 0;
            let mut max_span_cost = 0;
            while i + span_size < n && colors[i + span_size] == colors[i] {
                if needed_time[i + span_size] > max_span_cost {
                    max_span_cost = needed_time[i + span_size];
                }
                total_span_cost += needed_time[i + span_size];
                span_size += 1;
            }
            if span_size >= 2 {
                // We need to remove all but one balloons in a span. Leave the most expensive
                // balloon to minimize the time taken for balloon removal.
                total_cost += total_span_cost - max_span_cost;
            }
            i += span_size;
        }
        total_cost
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(
            Solution::min_cost("abaac".to_string(), vec![1, 2, 3, 4, 5]),
            3
        );
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::min_cost("abc".to_string(), vec![1, 2, 3]), 0);
    }

    #[test]
    fn test3() {
        assert_eq!(
            Solution::min_cost("aabaa".to_string(), vec![1, 2, 3, 4, 1]),
            2
        );
    }

    #[test]
    fn test4() {
        assert_eq!(
            Solution::min_cost("bbbaaa".to_string(), vec![4, 9, 3, 8, 8, 9]),
            23
        );
    }
}
