use crate::{ListNode, Solution};

/*
 * @lc app=leetcode id=21 lang=rust
 *
 * [21] Merge Two Sorted Lists
 *
 * https://leetcode.com/problems/merge-two-sorted-lists/description/
 *
 * algorithms
 * Easy (63.83%)
 * Likes:    21049
 * Dislikes: 2003
 * Total Accepted:    3.9M
 * Total Submissions: 6.1M
 * Testcase Example:  '[1,2,4]\n[1,3,4]'
 *
 * You are given the heads of two sorted linked lists list1 and list2.
 *
 * Merge the two lists into one sorted list. The list should be made by
 * splicing together the nodes of the first two lists.
 *
 * Return the head of the merged linked list.
 *
 *
 * Example 1:
 *
 *
 * Input: list1 = [1,2,4], list2 = [1,3,4]
 * Output: [1,1,2,3,4,4]
 *
 *
 * Example 2:
 *
 *
 * Input: list1 = [], list2 = []
 * Output: []
 *
 *
 * Example 3:
 *
 *
 * Input: list1 = [], list2 = [0]
 * Output: [0]
 *
 *
 *
 * Constraints:
 *
 *
 * The number of nodes in both lists is in the range [0, 50].
 * -100 <= Node.val <= 100
 * Both list1 and list2 are sorted in non-decreasing order.
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn merge_two_lists(
        list1: Option<Box<ListNode>>,
        list2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut result = Box::new(ListNode::new(0));
        let mut tail_ref = &mut result;
        let mut l1 = list1;
        let mut l2 = list2;
        loop {
            match (l1.clone(), l2.clone()) {
                (Some(ll1), Some(ll2)) => {
                    if ll1.val <= ll2.val {
                        tail_ref.next = l1;
                        l1 = ll1.next;
                    } else {
                        tail_ref.next = l2;
                        l2 = ll2.next;
                    }
                    tail_ref = tail_ref.next.as_mut().unwrap();
                }
                (Some(ll1), None) => {
                    tail_ref.next = l1;
                    l1 = ll1.next;
                    tail_ref = tail_ref.next.as_mut().unwrap();
                }
                (None, Some(ll2)) => {
                    tail_ref.next = l2;
                    l2 = ll2.next;
                    tail_ref = tail_ref.next.as_mut().unwrap();
                }
                (None, None) => {
                    break;
                }
            }
        }
        result.next
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        // Input: list1 = [1,2,4], list2 = [1,3,4]
        // Output: [1,1,2,3,4,4]
        let l1 = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode::new(4))),
                val: 2,
            })),
            val: 1,
        }));
        let l2 = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode::new(4))),
                val: 3,
            })),
            val: 1,
        }));
        let result = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode {
                    next: Some(Box::new(ListNode {
                        next: Some(Box::new(ListNode {
                            next: Some(Box::new(ListNode::new(4))),
                            val: 4,
                        })),
                        val: 3,
                    })),
                    val: 2,
                })),
                val: 1,
            })),
            val: 1,
        }));
        assert_eq!(Solution::merge_two_lists(l1, l2), result);
    }
}
