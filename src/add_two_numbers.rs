use crate::ListNode;
use crate::Solution;

/*
 * @lc app=leetcode id=2 lang=rust
 *
 * [2] Add Two Numbers
 *
 * https://leetcode.com/problems/add-two-numbers/description/
 *
 * algorithms
 * Medium (42.16%)
 * Likes:    29862
 * Dislikes: 5849
 * Total Accepted:    4.3M
 * Total Submissions: 10.2M
 * Testcase Example:  '[2,4,3]\n[5,6,4]'
 *
 * You are given two non-empty linked lists representing two non-negative
 * integers. The digits are stored in reverse order, and each of their nodes
 * contains a single digit. Add the two numbers and return the sum as a linked
 * list.
 *
 * You may assume the two numbers do not contain any leading zero, except the
 * number 0 itself.
 *
 *
 * Example 1:
 *
 *
 * Input: l1 = [2,4,3], l2 = [5,6,4]
 * Output: [7,0,8]
 * Explanation: 342 + 465 = 807.
 *
 *
 * Example 2:
 *
 *
 * Input: l1 = [0], l2 = [0]
 * Output: [0]
 *
 *
 * Example 3:
 *
 *
 * Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
 * Output: [8,9,9,9,0,0,0,1]
 *
 *
 *
 * Constraints:
 *
 *
 * The number of nodes in each linked list is in the range [1, 100].
 * 0 <= Node.val <= 9
 * It is guaranteed that the list represents a number that does not have
 * leading zeros.
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn add_two_numbers(
        l1: Option<Box<ListNode>>,
        l2: Option<Box<ListNode>>,
    ) -> Option<Box<ListNode>> {
        let mut carry = 0;
        let mut l1 = l1;
        let mut l2 = l2;
        let mut result = Box::new(ListNode::new(0));
        let mut tail_ref = &mut result;

        loop {
            match (l1.clone(), l2.clone()) {
                (None, None) => {
                    if carry > 0 {
                        tail_ref.next = Some(Box::new(ListNode::new(carry)));
                    }
                    break;
                }
                (Some(ll1), Some(ll2)) => {
                    let digit_val = (ll1.val + ll2.val + carry) % 10;
                    carry = (ll1.val + ll2.val + carry) / 10;
                    tail_ref.next = Some(Box::new(ListNode::new(digit_val)));
                    tail_ref = tail_ref.next.as_mut().unwrap();
                    l1 = ll1.next;
                    l2 = ll2.next;
                }
                (Some(ll1), None) => {
                    let digit_val = (ll1.val + carry) % 10;
                    carry = (ll1.val + carry) / 10;
                    tail_ref.next = Some(Box::new(ListNode::new(digit_val)));
                    tail_ref = tail_ref.next.as_mut().unwrap();
                    l1 = ll1.next;
                }
                (None, Some(ll2)) => {
                    let digit_val = (ll2.val + carry) % 10;
                    carry = (ll2.val + carry) / 10;
                    tail_ref.next = Some(Box::new(ListNode::new(digit_val)));
                    tail_ref = tail_ref.next.as_mut().unwrap();
                    l2 = ll2.next;
                }
            }
        }
        result.next
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        // Input: l1 = [2,4,3], l2 = [5,6,4]
        // Output: [7,0,8]
        // Explanation: 342 + 465 = 807.
        let l1 = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode::new(3))),
                val: 4,
            })),
            val: 2,
        }));
        let l2 = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode::new(4))),
                val: 6,
            })),
            val: 5,
        }));
        let result = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode::new(8))),
                val: 0,
            })),
            val: 7,
        }));
        assert_eq!(Solution::add_two_numbers(l1, l2), result);
    }

    #[test]
    fn test2() {
        // Input: l1 = [0], l2 = [0]
        // Output: [0]
        let l1 = Some(Box::new(ListNode::new(0)));
        let l2 = Some(Box::new(ListNode::new(0)));
        let result = Some(Box::new(ListNode::new(0)));
        assert_eq!(Solution::add_two_numbers(l1, l2), result);
    }

    #[test]
    fn test3() {
        // * Input: l1 = [9,9,9], l2 = [9]
        // * Output: [8,0,0,1]
        let l1 = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode::new(9))),
                val: 9,
            })),
            val: 9,
        }));
        let l2 = Some(Box::new(ListNode::new(9)));
        let result = Some(Box::new(ListNode {
            next: Some(Box::new(ListNode {
                next: Some(Box::new(ListNode {
                    next: Some(Box::new(ListNode::new(1))),
                    val: 0,
                })),
                val: 0,
            })),
            val: 8,
        }));
        assert_eq!(Solution::add_two_numbers(l1, l2), result);
    }
}
