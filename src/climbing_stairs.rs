use crate::Solution;

/*
 * @lc app=leetcode id=70 lang=rust
 *
 * [70] Climbing Stairs
 *
 * https://leetcode.com/problems/climbing-stairs/description/
 *
 * algorithms
 * Easy (52.80%)
 * Likes:    21361
 * Dislikes: 769
 * Total Accepted:    3.1M
 * Total Submissions: 5.8M
 * Testcase Example:  '2'
 *
 * You are climbing a staircase. It takes n steps to reach the top.
 *
 * Each time you can either climb 1 or 2 steps. In how many distinct ways can
 * you climb to the top?
 *
 *
 * Example 1:
 *
 *
 * Input: n = 2
 * Output: 2
 * Explanation: There are two ways to climb to the top.
 * 1. 1 step + 1 step
 * 2. 2 steps
 *
 *
 * Example 2:
 *
 *
 * Input: n = 3
 * Output: 3
 * Explanation: There are three ways to climb to the top.
 * 1. 1 step + 1 step + 1 step
 * 2. 1 step + 2 steps
 * 3. 2 steps + 1 step
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= n <= 45
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn climb_stairs(n: i32) -> i32 {
        let mut s: Vec<i32> = Vec::with_capacity((n + 3) as usize);
        s.resize((n + 3) as usize, 0);

        s[0] = 1;
        s[1] = 1;

        for i in 2..=n as usize {
            s[i] = s[i - 1] + s[i - 2];
        }
        s[n as usize]
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::climb_stairs(2), 2);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::climb_stairs(3), 3);
    }

    #[test]
    fn test3() {
        assert_eq!(Solution::climb_stairs(1), 1);
    }

    #[test]
    fn test4() {
        assert_eq!(Solution::climb_stairs(4), 5);
    }
}
