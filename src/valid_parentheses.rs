use crate::Solution;

/*
 * @lc app=leetcode id=20 lang=rust
 *
 * [20] Valid Parentheses
 *
 * https://leetcode.com/problems/valid-parentheses/description/
 *
 * algorithms
 * Easy (40.39%)
 * Likes:    23259
 * Dislikes: 1639
 * Total Accepted:    4.3M
 * Total Submissions: 10.5M
 * Testcase Example:  '"()"'
 *
 * Given a string s containing just the characters '(', ')', '{', '}', '[' and
 * ']', determine if the input string is valid.
 *
 * An input string is valid if:
 *
 *
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * Every close bracket has a corresponding open bracket of the same type.
 *
 *
 *
 * Example 1:
 *
 *
 * Input: s = "()"
 * Output: true
 *
 *
 * Example 2:
 *
 *
 * Input: s = "()[]{}"
 * Output: true
 *
 *
 * Example 3:
 *
 *
 * Input: s = "(]"
 * Output: false
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= s.length <= 10^4
 * s consists of parentheses only '()[]{}'.
 *
 *
 */

// @lc code=start
impl Solution {
    pub fn is_valid(s: String) -> bool {
        let mut stack = String::new();
        for ch in s.chars() {
            match ch {
                '(' | '{' | '[' => {
                    stack.push(ch);
                }
                ')' | '}' | ']' => {
                    if !is_matching_brackets(stack.pop(), ch) {
                        return false;
                    }
                }
                _ => {
                    unreachable!();
                }
            }
        }
        stack.is_empty()
    }
}

fn is_matching_brackets(l: Option<char>, r: char) -> bool {
    if let Some(ll) = l {
        match (ll, r) {
            ('(', ')') | ('{', '}') | ('[', ']') => true,
            _ => false,
        }
    } else {
        false
    }
}

// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert!(Solution::is_valid("()".to_string()));
    }

    #[test]
    fn test2() {
        assert!(Solution::is_valid("()[]{}".to_string()));
    }

    #[test]
    fn test3() {
        assert!(!Solution::is_valid("(]".to_string()));
    }

    #[test]
    fn test4() {
        assert!(!Solution::is_valid("[".to_string()));
    }

    #[test]
    fn test5() {
        assert!(!Solution::is_valid("]".to_string()));
    }
}
