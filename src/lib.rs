pub struct Solution;

mod add_two_numbers;
mod climbing_stairs;
mod find_the_index_of_the_first_occurrence_in_a_string;
mod house_robber;
mod longest_common_prefix;
mod longest_substring_without_repeating_characters;
mod merge_two_sorted_lists;
mod min_cost_climbing_stairs;
mod minimum_time_to_make_rope_colorful;
mod n_th_tribonacci_number;
mod palindrome_number;
mod perfect_squares;
mod remove_duplicates_from_sorted_array;
mod remove_element;
mod roman_to_integer;
mod search_insert_position;
mod two_sum;
mod valid_parentheses;

#[derive(PartialEq, Eq, Clone, Debug)]
pub struct ListNode {
    pub val: i32,
    pub next: Option<Box<ListNode>>,
}

impl ListNode {
    #[inline]
    fn new(val: i32) -> Self {
        ListNode { next: None, val }
    }
}
