use crate::Solution;

/*
 * @lc app=leetcode id=198 lang=rust
 *
 * [198] House Robber
 *
 * https://leetcode.com/problems/house-robber/description/
 *
 * algorithms
 * Medium (50.79%)
 * Likes:    20526
 * Dislikes: 404
 * Total Accepted:    2.1M
 * Total Submissions: 4.1M
 * Testcase Example:  '[1,2,3,1]'
 *
 * You are a professional robber planning to rob houses along a street. Each
 * house has a certain amount of money stashed, the only constraint stopping
 * you from robbing each of them is that adjacent houses have security systems
 * connected and it will automatically contact the police if two adjacent
 * houses were broken into on the same night.
 *
 * Given an integer array nums representing the amount of money of each house,
 * return the maximum amount of money you can rob tonight without alerting the
 * police.
 *
 *
 * Example 1:
 *
 *
 * Input: nums = [1,2,3,1]
 * Output: 4
 * Explanation: Rob house 1 (money = 1) and then rob house 3 (money = 3).
 * Total amount you can rob = 1 + 3 = 4.
 *
 *
 * Example 2:
 *
 *
 * Input: nums = [2,7,9,3,1]
 * Output: 12
 * Explanation: Rob house 1 (money = 2), rob house 3 (money = 9) and rob house
 * 5 (money = 1).
 * Total amount you can rob = 2 + 9 + 1 = 12.
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= nums.length <= 100
 * 0 <= nums[i] <= 400
 *
 *
 */

// @lc code=start
use std::cmp::max;

impl Solution {
    pub fn rob(nums: Vec<i32>) -> i32 {
        let n = nums.len();
        if n == 0 {
            return 0;
        } else if n == 1 {
            return nums[0];
        }
        let mut s = vec![0; n];
        s[0] = nums[0];
        s[1] = max(nums[0], nums[1]);
        for i in 2..n {
            s[i] = max(nums[i] + s[i - 2], s[i - 1]);
        }
        s[n - 1]
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::rob(vec![1, 2, 3, 1]), 4);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::rob(vec![2, 7, 9, 3, 1]), 12);
    }

    #[test]
    fn test3() {
        assert_eq!(Solution::rob(vec![1, 2, 3, 4, 5, 6, 7]), 16);
    }

    #[test]
    fn test4() {
        assert_eq!(Solution::rob(vec![10, 1, 1, 1, 1]), 12);
    }

    #[test]
    fn test5() {
        assert_eq!(Solution::rob(vec![1, 10, 1, 1, 1, 1]), 12);
    }

    #[test]
    fn test6() {
        assert_eq!(Solution::rob(vec![1, 10, 100, 1, 1, 1]), 102);
    }
}
