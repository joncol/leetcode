use crate::Solution;

/*
 * @lc app=leetcode id=35 lang=rust
 *
 * [35] Search Insert Position
 *
 * https://leetcode.com/problems/search-insert-position/description/
 *
 * algorithms
 * Easy (45.26%)
 * Likes:    15622
 * Dislikes: 695
 * Total Accepted:    2.7M
 * Total Submissions: 6M
 * Testcase Example:  '[1,3,5,6]\n5'
 *
 * Given a sorted array of distinct integers and a target value, return the
 * index if the target is found. If not, return the index where it would be if
 * it were inserted in order.
 *
 * You must write an algorithm with O(log n) runtime complexity.
 *
 *
 * Example 1:
 *
 *
 * Input: nums = [1,3,5,6], target = 5
 * Output: 2
 *
 *
 * Example 2:
 *
 *
 * Input: nums = [1,3,5,6], target = 2
 * Output: 1
 *
 *
 * Example 3:
 *
 *
 * Input: nums = [1,3,5,6], target = 7
 * Output: 4
 *
 *
 *
 * Constraints:
 *
 *
 * 1 <= nums.length <= 10^4
 * -10^4 <= nums[i] <= 10^4
 * nums contains distinct values sorted in ascending order.
 * -10^4 <= target <= 10^4
 *
 *
 */

// @lc code=start

impl Solution {
    pub fn search_insert(nums: Vec<i32>, target: i32) -> i32 {
        if nums.is_empty() {
            return -1;
        }
        if target < nums[0] {
            return 0;
        }
        let mut lo = 0;
        let mut hi = nums.len() - 1;
        let mut mid = lo;
        loop {
            if hi < lo {
                break;
            }
            mid = (hi - lo) / 2 + lo;
            if target == nums[mid] {
                return mid as i32;
            } else if target < nums[mid] {
                hi = mid - 1;
            } else if target > nums[mid] {
                lo = mid + 1;
            }
        }
        if target > nums[mid] {
            return (mid + 1) as i32;
        } else {
            return mid as i32;
        }
    }
}
// @lc code=end

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test1() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 5), 2);
    }

    #[test]
    fn test2() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 2), 1);
    }

    #[test]
    fn test3() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 7), 4);
    }

    #[test]
    fn test4() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 5), 2);
    }

    #[test]
    fn test5() {
        assert_eq!(Solution::search_insert(vec![1, 3, 5, 6], 0), 0);
    }

    #[test]
    fn test6() {
        assert_eq!(Solution::search_insert(vec![1, 3], 2), 1);
    }
}
